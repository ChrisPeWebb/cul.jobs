﻿using System;
using System.Collections.Generic;
using System.Threading;

using NUnit.Framework;

using UnityEngine;

using CUL.Jobs;

namespace CUL.Jobs.UnityTest
{
    [TestFixture]
    [Category("Job Manager Tests")]
    internal class ActionJobTest
    {
        // These tests must be run in a new scene each time (there is an option for that within the test runner

        [Test]
        public void ActionJobSimpleTest()
        {
            int someValueToChange = 0;

            JobManager.Instance.AddJob(() => { someValueToChange = 1; });


            TestUtils.SleepUntilConditionTrueOrTimeout(
            () =>
            {
                return someValueToChange == 1;
            });


            // Ensure that the value was changed on the job thread
            Assert.AreEqual(1, someValueToChange);

            GameObject.DestroyImmediate(JobManager.Instance.gameObject);
        }


        [Test]
        public void ActionJobCompleteEventTest()
        {
            int someValueToChange = 0;
            bool didComplete = false;

            JobManager.Instance.AddJob(() => { someValueToChange = 1; }, (IJob a_job) => { didComplete = true; });


            TestUtils.SleepUntilConditionTrueOrTimeout(
            () =>
            {
                return (someValueToChange == 1 && didComplete);
            });

            // Ensure that the value was changed on the job thread
            Assert.AreEqual(1, someValueToChange);

            // Ensure the complete event was fired
            Assert.AreEqual(true, didComplete);

            GameObject.DestroyImmediate(JobManager.Instance.gameObject);
        }


        [Test]
        public void ActionJobLightSaturationTest()
        {
            object locker = new object();

            const int jobsToTest = 1000;

            int someValueToChange = 0;
            int actionsCompleted = 0;

            // Create several jobs which increment a value while working and while firing the completed event
            for(int i = 0; i < jobsToTest; ++i)
            {
                JobManager.Instance.AddJob(
                () =>
                {
                    lock(locker)
                    {
                        ++someValueToChange;
                    }
                },
                (IJob a_job) =>
                {
                    lock(locker)
                    {
                        ++actionsCompleted;
                    }
                });
            }

            // Sleep until we have either slept far too much (indicating there is a big problem) or the work has all been done
            const int maxSleeps = 100;
            int totalSleeps = 0;
            while(totalSleeps < maxSleeps && (someValueToChange != jobsToTest || actionsCompleted != jobsToTest))
            {
                ++totalSleeps;
                Thread.Sleep(1000);
            }


            TestUtils.SleepUntilConditionTrueOrTimeout(
            () =>
            {
                return (someValueToChange == jobsToTest && actionsCompleted == jobsToTest);
            });


            // Ensure all of the jobs were executed
            Assert.AreEqual(jobsToTest, someValueToChange);

            // Ensure all of the jobs completed
            Assert.AreEqual(jobsToTest, actionsCompleted);

            GameObject.DestroyImmediate(JobManager.Instance.gameObject);
        }


        [Test]
        public void ActionJobHeavySaturationTest()
        {
            object locker = new object();

            const int jobsToTest = 5000;

            int someValueToChange = 0;
            int actionsCompleted = 0;

            // Create several jobs which increment a value while working and while firing the completed event
            for(int i = 0; i < jobsToTest; ++i)
            {
                JobManager.Instance.AddJob(
                () =>
                {
                    // perform some random work to keep the thread busy for a little bit (because sleeping is cheating)
                    for(int a = 0; a < 1500; ++a)
                    {
                        for(int b = 0; b < 1500; ++b)
                        {
                            int someNumber = a ^ b ^ a ^ b;
                        }
                    }

                    lock(locker)
                    {
                        ++someValueToChange;
                    }
                },
                (IJob a_job) =>
                {
                    lock(locker)
                    {
                        ++actionsCompleted;
                    }
                });
            }


            TestUtils.SleepUntilConditionTrueOrTimeout(
            () =>
            {
                return (someValueToChange == jobsToTest && actionsCompleted == jobsToTest);
            });


            // Ensure all of the jobs were executed
            Assert.AreEqual(jobsToTest, someValueToChange);

            // Ensure all of the jobs completed
            Assert.AreEqual(jobsToTest, actionsCompleted);

            GameObject.DestroyImmediate(JobManager.Instance.gameObject);
        }


        [Test]
        public void ActionJobMainThreadFallbackTest()
        {
            object locker = new object();

            const int maxQueueSize = 5000;

            int someValueToChange = 0;
            int actionsCompleted = 0;
            int jobsRunOnMainThread = 0;

            bool jobsCanFinish = false;

            int jobsMade = 0;

            // in order to get a job to be forcefully run on the main thread, we need to add enough jobs to saturate the workers (JobThreadCount), then enough to saturate the queue (maxQueueSize)
            // then another to be forced onto the main thread. After this, we will release the jobs that were sleeping
            for(int i = 0; i < maxQueueSize + JobManager.Instance.JobThreadCount + 1; ++i)
            {
                ++jobsMade;

                JobManager.Instance.AddJob(
                () =>
                {
                    // If this job is being run on the main thread, everything is okay and we can finish the rest of the jobs
                    if(JobManager.CheckIfMainThread())
                    {
                        lock(locker)
                        {
                            jobsCanFinish = true;
                            ++jobsRunOnMainThread;
                        }
                    }

                    // Threads will sleep until they are told to complete. This is so we can add max the queue without competing with threads consuming from it
                    while(!jobsCanFinish)
                    {
                        Thread.Sleep(50);
                    }

                    lock(locker)
                    {
                        ++someValueToChange;
                    }
                },
                (IJob a_job) =>
                {
                    lock(locker)
                    {
                        ++actionsCompleted;
                    }
                });
            }

            
            TestUtils.SleepUntilConditionTrueOrTimeout(
            () =>
            {
                return (someValueToChange == jobsMade && actionsCompleted == jobsMade);
            });


            // Ensure only one job was run on the main thread
            Assert.AreEqual(jobsRunOnMainThread, 1);

            // Ensure all of the jobs were executed
            Assert.AreEqual(jobsMade, someValueToChange);

            // Ensure all of the jobs completed
            Assert.AreEqual(jobsMade, actionsCompleted);

            GameObject.DestroyImmediate(JobManager.Instance.gameObject);
        }
    }
}
