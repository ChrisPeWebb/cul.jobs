﻿using System;
using System.Collections.Generic;
using System.Threading;

using NUnit.Framework;

using UnityEngine;

using CUL.Jobs;

namespace CUL.Jobs.UnityTest
{
    [TestFixture]
    [Category("Job Manager Tests")]
    internal class JobManagerTest
    {
        // These tests must be run in a new scene each time (there is an option for that within the test runner

        [Test]
        public void InitializationTest()
        {
            JobManager.Instance.Initialize();

            // Ensure the instance was created
            Assert.AreNotEqual(null, JobManager.Instance);

            // Ensure threads were created (depends on the current cpu, but should be more than none)
            Assert.AreNotEqual(0, JobManager.Instance.JobThreadCount);

            // Ensure we initialize with no jobs
            Assert.AreEqual(0, JobManager.Instance.JobCount);

            // Ensure all threads are idle as they start
            Assert.AreEqual(JobManager.Instance.JobThreadCount, JobManager.Instance.GetIdleThreadsCount());

            // Ensure no threads are busy
            Assert.AreEqual(0, JobManager.Instance.GetBusyThreadsCount());

            GameObject.DestroyImmediate(JobManager.Instance.gameObject);
        }


        [Test]
        public void LifeCycleTest()
        {
            // Ensure when not quitting, we dont think we are quitting
            Assert.AreEqual(false, JobManager.IsApplicationQuitting);

            // Ensure when not paused and not quitting that we think we are active
            Assert.AreEqual(true, JobManager.IsApplicationActive);

            // Currently its pretty hard to test the rest of the activity with a unit test alone, as it requires a frame to propagate the info to the game objects

            GameObject.DestroyImmediate(JobManager.Instance.gameObject);
        }
    }
}
