﻿using System;
using System.Threading;

using NUnit.Framework;

namespace CUL.Jobs.UnityTest
{
    public static class TestUtils
    {
        /// <summary>
        /// Sleeps a thread until it times out or until a condition has been satisfied
        /// </summary>
        public static void SleepUntilConditionTrueOrTimeout(Func<bool> a_condition, int a_sleepTime = 500, int a_maxSleeps = 100)
        {
            int totalSleeps = 0;
            while(totalSleeps < a_maxSleeps && !a_condition())
            {
                ++totalSleeps;
                Thread.Sleep(a_sleepTime);
            }

            // Ensure we didnt max out the sleeps
            Assert.Less(totalSleeps, a_maxSleeps, "Sleep timeout has been reached. Condition took longer than expected to be satisfied, or will never be satisfied.");
        }
    }
}