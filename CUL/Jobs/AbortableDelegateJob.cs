﻿using System;

namespace CUL.Jobs
{
    public delegate void AbortableJobDelegate(ref bool a_isAborted);

    /// <summary>
    /// A job that proceses a delegate.
    /// The delegate recieves information about whether to abort or not, so that execution can stop when required.
    /// </summary>
    public class AbortableDelegateJob : IJob
    {
        private AbortableJobDelegate m_jobDelegate;

        private bool m_isAborted;

        public AbortableDelegateJob(AbortableJobDelegate a_jobDelegate)
        {
            m_jobDelegate = a_jobDelegate;
        }


        public void Execute()
        {
            m_jobDelegate(ref m_isAborted);
        }


        public void Abort()
        {
            m_isAborted = true;
        }
    }
}