﻿using System;

namespace CUL.Jobs
{
    /// <summary>
    /// A Job which processes a simple action. Ideally used for small actions.
    /// For any jobs that may hold a thread for more than a 100 ms, consider using an AbortableDelegateJob
    /// or create a Job using the IJob interface and submit that to the JobManager instead.
    /// </summary>
    public class ActionJob : IJob
    {
        private Action m_action;

        public ActionJob(Action a_action)
        {
            m_action = a_action;
        }


        public void Execute()
        {
            m_action();
        }


        public void Abort()
        {
        }
    }
}