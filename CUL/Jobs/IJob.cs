﻿
namespace CUL.Jobs
{
    /// <summary>
    /// Defines the interface for a job which will be run on a thread to complete work
    /// </summary>
    public interface IJob 
    {
        void Execute();

        void Abort();
    }
}
