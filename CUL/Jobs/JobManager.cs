﻿using UnityEngine;

using System;
using System.Threading;
using System.Collections.Generic;

using CUL.Jobs.Collections;
using CUL.Jobs.Internal;

namespace CUL.Jobs
{
    /// <summary>
    /// Occures when a jobs execution has completed.
    /// </summary>
    /// <param name="finishedJob">The job which was completed.</param>
    public delegate void OnJobCompleteEvent(IJob finishedJob);

    /// <summary>
    /// Manages a job queue and a number of job threads which execute work from the queue. Exposes information about the applications current state.
    /// </summary>
    public class JobManager : MonoBehaviour
    {
        private const int k_maxQueueSize = 5000;

        [SerializeField]
        private int m_maxThreads = -1;

        [SerializeField]
        private bool m_executeOnMainThread = false;

        [SerializeField]
        private bool m_forceExceptionHandling = false;

        private static JobManager m_instance = null;

        private readonly static object m_lock = new object();

        private BlockingQueue<JobPackage> m_jobQueue = new BlockingQueue<JobPackage>(k_maxQueueSize);

        private JobThread[] m_jobThreads;

        private bool m_hasInitialized = false;

        private static bool m_applicationIsQuitting = false;
        private static bool m_applicationPaused = false;
        private static bool m_applicationFocused = true;
        private static bool m_applicationActive = true;

        /// <summary>
        /// Returns whether or not the application is currently quitting.
        /// </summary>
        public static bool IsApplicationQuitting { get { return m_applicationIsQuitting; } }

        /// <summary>
        /// Returns true when the application is not quitting, is focused and is not paused.
        /// </summary>
        public static bool IsApplicationActive { get { return m_applicationActive; } }

        /// <summary>
        /// Returns how many jobs are in the job queue. As the data is accessed from multiple threads, this data is probably out of data by the time it has been returned. Use with caution.
        /// </summary>
        public int JobCount { get { return m_jobQueue.Count; } }

        /// <summary>
        /// Returns how many job worker threads have been created.
        /// </summary>
        public int JobThreadCount { get { return (m_jobThreads != null) ? m_jobThreads.Length : 0; } }

        /// <summary>
        /// Returns an instance of the JobManager.
        /// </summary>
        public static JobManager Instance
        {
            get
            {
                lock(m_lock)
                {
                    if(m_applicationIsQuitting)
                    {
                        return null;
                    }

                    if(m_instance == null)
                    {
                        m_instance = (JobManager)GameObject.FindObjectOfType(typeof(JobManager));

                        if(FindObjectsOfType(typeof(JobManager)).Length > 1)
                        {
                            Debug.LogError("More than one job manager exists. This should never be the case. Reopening the scene might solve this.");
                            return m_instance;
                        }


                        if(m_instance == null)
                        {
                            GameObject singleton = new GameObject();
                            m_instance = singleton.AddComponent<JobManager>();
                            m_instance.Initialize();
                            singleton.name = typeof(JobManager).ToString();

                            DontDestroyOnLoad(singleton);

                            Debug.Log("Job Manager instance created");
                        }
                        else
                        {
                            DontDestroyOnLoad(m_instance.gameObject);
                        }
                    }

                    return m_instance;
                }
            }
        }


        /// <summary>
        /// Initializes the JobManager instance if it has not already been.
        /// </summary>
        public void Initialize()
        {
            if(m_hasInitialized)
            {
                return;
            }

            MainThreadWatchdog.Initialize();

            CreateJobThreads();

            m_hasInitialized = true;
        }


        /// <summary>
        /// <para>Adds a prioritizable job to the job queue via the IJob interface. When the job has been completed, it will be returned via <paramref name="a_onJobCompleteEvent"/>.</para>
        /// <para><paramref name="a_onJobCompleteEvent"/> can optionally be returned on the main thread.</para>
        /// <para>Exception handling can also be turned off for a minimal performance increase, but at the risk of locking the thread with no output.</para>
        /// </summary>
        /// <param name="a_job">The job which will be executed on a job thread.</param>
        /// <param name="a_onJobCompleteEvent">The event which will be fired when job execution has been completed. Can optionally be executed on the main thread using the <paramref name="a_returnOnMainThread"/> parameter.</param>
        /// <param name="a_returnOnMainThread">Fires <paramref name="a_onJobCompleteEvent"/> on the main thread automatically when job execution has completed.</param>
        /// <param name="a_priority">The priority of the job. Lower priority jobs will be executed first.</param>
        /// <param name="a_exceptionHandling">The job will be executed in a try/catch block, and any exceptions will be dispatched to unity.</param>
        public void AddJob(IJob a_job, OnJobCompleteEvent a_onJobCompleteEvent = null, bool a_returnOnMainThread = false, double a_priority = 1.0, bool a_exceptionHandling = true)
        {
            lock(m_lock)
            {
                if( m_jobQueue.Count >= k_maxQueueSize)
                {
                    Debug.LogError("Failed to add job to job queue. Max queue size has been reached. Job will be executed on main thread. Consider increasing k_maxQueueSize or not spamming the job manager so much.");
                    ExecuteJobOnMainThread(new JobPackage(a_job, a_onJobCompleteEvent, a_returnOnMainThread, a_exceptionHandling || m_forceExceptionHandling));
                    return;
                }

                if(m_executeOnMainThread)
                {
                    ExecuteJobOnMainThread(new JobPackage(a_job, a_onJobCompleteEvent, a_returnOnMainThread, a_exceptionHandling || m_forceExceptionHandling));
                }
                else
                {
                    m_jobQueue.Enqueue(new JobPackage(a_job, a_onJobCompleteEvent, a_returnOnMainThread, a_exceptionHandling || m_forceExceptionHandling), a_priority);
                }
            }
        }


        /// <summary>
        /// <para>Adds a prioritizable job to the job queue via a delegate which recieves abort information. When the job has been completed, it will be returned via <paramref name="a_onJobCompleteEvent"/>.</para>
        /// <para><paramref name="a_onJobCompleteEvent"/> can optionally be returned on the main thread.</para>
        /// <para>Exception handling can also be turned off for a minimal performance increase, but at the risk of locking the thread with no output.</para>
        /// </summary>
        /// <param name="a_jobDelegate">The delegate which will be executed on a job thread. Use the <paramref name="a_isAborted"/> parameter to stop execution if requested.</param>
        /// <param name="a_onJobCompleteEvent">The event which will be fired when job execution has been completed. Can optionally be executed on the main thread using the <paramref name="a_returnOnMainThread"/> parameter.</param>
        /// <param name="a_returnOnMainThread">Fires <paramref name="a_onJobCompleteEvent"/> on the main thread automatically when job execution has completed.</param>
        /// <param name="a_priority">The priority of the job. Lower priority jobs will be executed first.</param>
        /// <param name="a_exceptionHandling">The job will be executed in a try/catch block, and any exceptions will be dispatched to unity.</param>
        public void AddJob(AbortableJobDelegate a_jobDelegate, OnJobCompleteEvent a_onJobCompleteEvent = null, bool a_returnOnMainThread = false, double a_priority = 1.0, bool a_exceptionHandling = true)
        {
            lock(m_lock)
            {
                if(m_jobQueue.Count >= k_maxQueueSize)
                {
                    Debug.LogError("Failed to add job to job queue. Max queue size has been reached. Job will be executed on main thread. Consider increasing k_maxQueueSize or not spamming the job manager so much.");
                    ExecuteJobOnMainThread(new JobPackage(new AbortableDelegateJob(a_jobDelegate), a_onJobCompleteEvent, a_returnOnMainThread, a_exceptionHandling || m_forceExceptionHandling));
                    return;
                }

                if(m_executeOnMainThread)
                {
                    ExecuteJobOnMainThread(new JobPackage(new AbortableDelegateJob(a_jobDelegate), a_onJobCompleteEvent, a_returnOnMainThread, a_exceptionHandling || m_forceExceptionHandling));
                }
                else
                {
                    m_jobQueue.Enqueue(new JobPackage(new AbortableDelegateJob(a_jobDelegate), a_onJobCompleteEvent, a_returnOnMainThread, a_exceptionHandling || m_forceExceptionHandling), a_priority);
                }
            }
        }


        /// <summary>
        /// <para>The simplest but least ideal way to add a job. Adds a prioritizable job to the job queue via an action. When the job has been completed, it will be returned via <paramref name="a_onJobCompleteEvent"/>.</para>
        /// <para><paramref name="a_onJobCompleteEvent"/> can optionally be returned on the main thread.</para>
        /// <para>Exception handling can also be turned off for a minimal performance increase, but at the risk of locking the thread with no output.</para>
        /// </summary>
        /// <param name="a_action">The action that will be executed on a job thread. The action cannot be aborted, so ideally should be very small.</param>
        /// <param name="a_onJobCompleteEvent">The event which will be fired when job execution has been completed. Can optionally be executed on the main thread using the <paramref name="a_returnOnMainThread"/> parameter.</param>
        /// <param name="a_returnOnMainThread">Fires <paramref name="a_onJobCompleteEvent"/> on the main thread automatically when job execution has completed.</param>
        /// <param name="a_priority">The priority of the job. Lower priority jobs will be executed first.</param>
        /// <param name="a_exceptionHandling">The job will be executed in a try/catch block, and any exceptions will be dispatched to unity.</param>
        public void AddJob(Action a_action, OnJobCompleteEvent a_onJobCompleteEvent = null, bool a_returnOnMainThread = false, double a_priority = 1.0, bool a_exceptionHandling = true)
        {
            lock(m_lock)
            {
                if(m_jobQueue.Count >= k_maxQueueSize)
                {
                    Debug.LogError("Failed to add job to job queue. Max queue size has been reached. Job will be executed on main thread. Consider increasing k_maxQueueSize or not spamming the job manager so much.");
                    ExecuteJobOnMainThread(new JobPackage(new ActionJob(a_action), a_onJobCompleteEvent, a_returnOnMainThread, a_exceptionHandling || m_forceExceptionHandling));
                    return;
                }

                if(m_executeOnMainThread)
                {
                    ExecuteJobOnMainThread(new JobPackage(new ActionJob(a_action), a_onJobCompleteEvent, a_returnOnMainThread, a_exceptionHandling || m_forceExceptionHandling));
                }
                else
                {
                    m_jobQueue.Enqueue(new JobPackage(new ActionJob(a_action), a_onJobCompleteEvent, a_returnOnMainThread, a_exceptionHandling || m_forceExceptionHandling), a_priority);
                }
            }
        }


        /// <summary>
        /// <para>A helper method which can be used from within threads to sleep the thread if the application is not active.</para> 
        /// <para>Ideally long running jobs should call this regularly so that iOS does not kill the application if backgrounded.</para>
        /// </summary>
        /// <param name="a_sleepMilliseconds">How many milliseconds to sleep for at a time</param>
        public static void SleepIfApplicationIsInactive(int a_sleepMilliseconds = 100)
        {
            if(!MainThreadWatchdog.CheckIfMainThread())
            {
                while(!IsApplicationActive)
                {
                    Thread.Sleep(a_sleepMilliseconds);
                }
            }
        }


        /// <summary>
        /// Wraps MainThreadDispatcher. Dispatches a delegate to the main thread. The current thread can be made to wait until execution has completed before continuing.
        /// </summary>
        /// <param name="a_dispatchDelegate">The delegate to execute on the main thread.</param>
        /// <param name="a_waitForExecution">The current thread will be slept until execution of the delegate has completed.</param>
        /// <param name="a_exceptionHandling">Execution of the delegate will be wraped in a try/catch block, and any exceptions will be dispatched to unity.</param>
        public static void DispatchToMainThread(DispatchDelegate a_dispatchDelegate, bool a_waitForExecution = false, bool a_exceptionHandling = true)
        {
            MainThreadDispatcher.DispatchToMainThread(a_dispatchDelegate, a_waitForExecution, a_exceptionHandling);
        }


        /// <summary>
        /// Wraps MainThreadDispatcher. Dispatches a parameterized delegate to the main thread. The current thread can be made to wait until execution has completed before continuing.
        /// </summary>
        /// <param name="a_dispatchDelegate">The delegate to execute on the main thread.</param>
        /// <param name="a_argument">The argument which will be passed to the delegate.</param>
        /// <param name="a_waitForExecution">The current thread will be slept until execution of the delegate has completed.</param>
        /// <param name="a_exceptionHandling">Execution of the delegate will be wraped in a try/catch block, and any exceptions will be dispatched to unity.</param>
        public static void DispatchToMainThread(ParameterizedDispatchDelegate a_dispatchDelegate, object a_argument, bool a_waitForExecution = false, bool a_exceptionHandling = true)
        {
            MainThreadDispatcher.DispatchToMainThread(a_dispatchDelegate, a_argument, a_waitForExecution, a_exceptionHandling);
        }


        /// <summary>
        /// Wraps MainThreadDispatcher. Dispatches a returning delegate to the main thread. The current thread will be made to wait until execution has completed before continuing.
        /// </summary>
        /// <param name="a_dispatchDelegate">The delegate to execute on the main thread.</param>
        /// <param name="a_exceptionHandling">Execution of the delegate will be wraped in a try/catch block, and any exceptions will be dispatched to unity.</param>
        /// <returns>The result of the delegates execution.</returns>
        public static object DispatchToMainThreadReturn(ReturningDispatchDelegate a_dispatchDelegate, bool a_exceptionHandling = true)
        {
            return MainThreadDispatcher.DispatchToMainThreadReturn(a_dispatchDelegate, a_exceptionHandling);
        }


        /// <summary>
        /// Wraps MainThreadDispatcher. Dispatches a returning parameterized delegate to the main thread. The current thread will be made to wait until execution has completed before continuing.
        /// </summary>
        /// <param name="a_dispatchDelegate">The delegate to execute on the main thread.</param>
        /// <param name="a_argument">The argument which will be passed to the delegate.</param>
        /// <param name="a_exceptionHandling">Execution of the delegate will be wraped in a try/catch block, and any exceptions will be dispatched to unity.</param>
        /// <returns>The result of the delegates execution.</returns>
        public static object DispatchToMainThreadReturn(ParameterizedReturningDispatchDelegate a_dispatchDelegate, object a_argument, bool a_exceptionHandling = true)
        {
            return MainThreadDispatcher.DispatchToMainThreadReturn(a_dispatchDelegate, a_argument, a_exceptionHandling);
        }


        /// <summary>
        /// Wraps MainThreadWatchdog. Checks if the current thread is the main(Unity) thread.
        /// </summary>
        /// <returns>Whether or not the current thread is the main thread.</returns>
        public static bool CheckIfMainThread()
        {
            return MainThreadWatchdog.CheckIfMainThread();
        }


        private void Awake()
        {
            if(m_instance == null)
            {
                m_instance = this;
                DontDestroyOnLoad(this);
                Initialize();
            }
            else if(m_instance == this)
            {
                Initialize();
            }
            else
            {
                Debug.LogError("Job Manager instance destroyed. Singleton instance already exists.");
                DestroyImmediate(this.gameObject);
            }
        }


        private void Update()
        {
            MainThreadDispatcher.ProcessDispatchPackages();
        }


        private void OnApplicationPause(bool a_pauseState)
        {
            m_applicationPaused = a_pauseState;
            UpdateApplicationActiveState();
        }


        private void OnApplicationFocus(bool a_focusState)
        {
            m_applicationFocused = a_focusState;
            UpdateApplicationActiveState();
        }


        private void OnApplicationQuit()
        {
            m_applicationIsQuitting = true;

            UpdateApplicationActiveState();

            AbortJobExecution();
        }


        private void OnDestroy()
        {
            if(m_instance == this)
            {
                m_instance = null;

                if(!m_applicationIsQuitting)
                {
                    Debug.LogError("Job Manager instance being destroyed. Any running jobs will be aborted, and any queued jobs will be lost.");
                }
            }

            AbortJobExecution();
        }


        private void UpdateApplicationActiveState()
        {
#if UNITY_IPHONE
            m_applicationActive = !m_applicationIsQuitting && m_applicationFocused && !m_applicationPaused;
#elif UNITY_ANDROID
            m_applicationActive = !m_applicationIsQuitting && m_applicationFocused && !m_applicationPaused;
#else
            m_applicationActive = !m_applicationIsQuitting && !m_applicationPaused;
#endif
        }


        private void ExecuteJobOnMainThread(JobPackage a_jobPackage)
        {
            if(MainThreadWatchdog.CheckIfMainThread())
            {
                a_jobPackage.Job.Execute();

                if(a_jobPackage.CompleteEvent != null)
                {
                    a_jobPackage.CompleteEvent(a_jobPackage.Job);
                }
            }
            else
            {
                MainThreadDispatcher.DispatchToMainThread(
                () =>
                {
                    a_jobPackage.Job.Execute();

                    if(a_jobPackage.CompleteEvent != null)
                    {
                        a_jobPackage.CompleteEvent(a_jobPackage.Job);
                    }
                });
            }
        }


        private void CreateJobThreads()
        {
            if(m_maxThreads <= 0)
            {
                m_maxThreads = Mathf.Max(SystemInfo.processorCount - 1, 1);
            }

            Debug.Log("Creating job threads: " + m_maxThreads);

            m_jobThreads = new JobThread[m_maxThreads];

            for(int i = 0; i < m_maxThreads; ++i)
            {
                JobThread jobThread = new JobThread(m_jobQueue);
                m_jobThreads[i] = jobThread;
            }
        }


        private void AbortJobExecution()
        {
            lock(m_lock)
            {
                if(m_jobQueue != null)
                {
                    m_jobQueue.Stop();
                }

                if(m_jobThreads != null)
                {
                    for(int i = 0; i < m_jobThreads.Length; ++i)
                    {
                        m_jobThreads[i].StopJobExecution(true);
                    }
                }

                m_jobQueue = null;

                m_jobThreads = null;
            }
        }

        
        /// <summary>
        /// Gets how many threads are currently idle.
        /// </summary>
        /// <returns>How many threads are currently idle.</returns>
        public int GetIdleThreadsCount()
        {
            int idleThreads = 0;
            for(int i = 0; i < m_jobThreads.Length; ++i)
            {
                if(!m_jobThreads[i].IsBusy)
                {
                    ++idleThreads;
                }
            }
            return idleThreads;
        }


        /// <summary>
        /// Gets how many threads are currently busy.
        /// </summary>
        /// <returns>How many threads are currently busy.</returns>
        public int GetBusyThreadsCount()
        {
            int busyThreads = 0;
            for(int i = 0; i < m_jobThreads.Length; ++i)
            {
                if(m_jobThreads[i].IsBusy)
                {
                    ++busyThreads;
                }
            }
            return busyThreads;
        }


        /// <summary>
        /// Gets an array which contains whether or not each thread is busy.
        /// </summary>
        /// <returns>An array with each threads busy state.</returns>
        public bool[] GetBusyThreads()
        {
            bool[] busyThreads = new bool[m_jobThreads.Length];

            for(int i = 0; i < m_jobThreads.Length; ++i)
            {
                busyThreads[i] = m_jobThreads[i].IsBusy;
            }

            return busyThreads;
        }


        /// <summary>
        /// Gets an array of each threads current job name.
        /// </summary>
        /// <returns>An array of each threads job name.</returns>
        public string[] GetJobNames()
        {
            string[] jobNames = new string[m_jobThreads.Length];

            for(int i = 0; i < m_jobThreads.Length; ++i)
            {
                jobNames[i] = m_jobThreads[i].JobName;
            }

            return jobNames;
        }
    }
}
