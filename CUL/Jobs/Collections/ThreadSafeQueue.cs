﻿using System.Collections.Generic;

namespace CUL.Jobs.Collections
{ 
    /// <summary>
    /// A simple thread safe queue which uses the "try" collection pattern.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public sealed class ThreadSafeQueue<T>
    {
        private Queue<T> m_queue = new Queue<T>();

        private object m_lock = new object();


        /// <summary>
        /// Enqueues an item onto the queue.
        /// </summary>
        /// <param name="value">The item to enqueues.</param>
        public void Enqueue(T value)
        {
            lock(m_lock)
            {
                m_queue.Enqueue(value);
            }
        }


        /// <summary>
        /// Attempts to dequeue an item from the queue. If there are not items to dequeue, will return false and out as default T (null).
        /// </summary>
        /// <param name="value">The item that will be set if dequeue is successful. otherwise will be set to default T (null). </param>
        /// <returns>Whether or not an item was successfully dequeued.</returns>
        public bool TryDequeue(out T value)
        {
            lock(m_lock)
            {
                if(m_queue.Count > 0)
                {
                    value = m_queue.Dequeue();
                    return true;
                }
                value = default(T);
                return false;
            }
        }


        /// <summary>
        /// Clears the queue
        /// </summary>
        public void Clear()
        {
            lock(m_lock)
            {
                m_queue.Clear();
            }
        }
    }
}
