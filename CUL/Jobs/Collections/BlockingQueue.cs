﻿using System.Collections.Generic;
using System.Threading;

namespace CUL.Jobs.Collections
{
    /// <summary>
    /// A simple blocking queue. If there is nothing to dequeue, threads will wait until they are pulsed by an enqueue.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class BlockingQueue<T> where T : PriorityQueueNode
    {
        private readonly HeapPriorityQueue<T> m_queue;

        private object m_lock = new object();

        private bool m_stopped;


        /// <summary>
        /// Initialized the internal queue with a max size.
        /// </summary>
        /// <param name="a_queueMaxSize"></param>
        public BlockingQueue(int a_queueMaxSize)
        {
            m_queue = new HeapPriorityQueue<T>(a_queueMaxSize);
        }


        /// <summary>
        /// Enqueues a prioritized item into the internal queue. A thread waiting to dequeue will be pulsed.
        /// </summary>
        /// <param name="a_item"></param>
        /// <param name="a_priority"></param>
        /// <returns></returns>
        public bool Enqueue(T a_item, double a_priority = 1.0)
        {
            if(m_stopped)
            {
                return false;
            }

            lock(m_lock)
            {
                if(m_stopped)
                {
                    return false;
                }

                m_queue.Enqueue(a_item, a_priority);
                Monitor.Pulse(m_lock);
            }
            return true;
        }


        /// <summary>
        /// Dequeues an item from the internal queue. If there is no item to dequeue, the thread will sleep until it is pulsed by an enqueue.
        /// </summary>
        /// <returns></returns>
        public T Dequeue()
        {
            if(m_stopped)
            {
                return default(T);
            }

            lock(m_lock)
            {
                if(m_stopped)
                {
                    return default(T);
                }

                while(m_queue.Count == 0)
                {
                    Monitor.Wait(m_lock);

                    if(m_stopped)
                    {
                        return default(T);
                    }
                }
                return m_queue.Dequeue();
            }
        }


        /// <summary>
        /// Any thread waiting to dequeue will return null. This effectively destroyed the queue, which then requires reconstruction.
        /// </summary>
        public void Stop()
        {
            if(m_stopped)
            {
                return;
            }

            lock(m_lock)
            {
                if(m_stopped)
                {
                    return;
                }

                m_stopped = true;
                Monitor.PulseAll(m_lock);
            }
        }


        /// <summary>
        /// Returns the current count of the queue. As the data is accessed from multiple threads, this data is probably out of data by the time it has been returned. Use with caution.
        /// </summary>
        public int Count
        {
            get
            {
                return m_queue.Count;
            }
        }
    }
}