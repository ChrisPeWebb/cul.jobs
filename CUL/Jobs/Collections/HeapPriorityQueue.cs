﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace CUL.Jobs.Collections
{
    /// <summary>
    /// An implementation of a min-Priority Queue using a heap.  Has O(1) .Contains()!
    /// </summary>
    /// <typeparam name="T">The values in the queue.  Must implement the PriorityQueueNode interface</typeparam>
    public sealed class HeapPriorityQueue<T> : IPriorityQueue<T> where T : PriorityQueueNode
    {
        private int m_numNodes;
        private readonly T[] m_nodes;
        private long m_numNodesEverEnqueued;


        /// <summary>
        /// Instantiate a new Priority Queue
        /// </summary>
        /// <param name="a_maxNodes">The max nodes ever allowed to be enqueued (going over this will cause an exception)</param>
        public HeapPriorityQueue(int a_maxNodes)
        {
            m_numNodes = 0;
            m_nodes = new T[a_maxNodes + 1];
            m_numNodesEverEnqueued = 0;
        }


        /// <summary>
        /// Returns the number of nodes in the queue.  O(1)
        /// </summary>
        public int Count
        {
            get
            {
                return m_numNodes;
            }
        }


        /// <summary>
        /// Returns the maximum number of items that can be enqueued at once in this queue.  Once you hit this number (ie. once Count == MaxSize),
        /// attempting to enqueue another item will throw an exception.  O(1)
        /// </summary>
        public int MaxSize
        {
            get
            {
                return m_nodes.Length - 1;
            }
        }


        /// <summary>
        /// Removes every node from the queue.  O(n) (So, don't do this often!)
        /// </summary>
        #if NET_VERSION_4_5
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        #endif
        public void Clear()
        {
            Array.Clear(m_nodes, 1, m_numNodes);
            m_numNodes = 0;
        }


        /// <summary>
        /// Returns (in O(1)!) whether the given node is in the queue.  O(1)
        /// </summary>
        #if NET_VERSION_4_5
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        #endif
        public bool Contains(T a_node)
        {
            return (m_nodes[a_node.QueueIndex] == a_node);
        }


        /// <summary>
        /// Enqueue a node - .Priority must be set beforehand!  O(log n)
        /// </summary>
        #if NET_VERSION_4_5
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        #endif
        public void Enqueue(T a_node, double a_priority)
        {
            a_node.Priority = a_priority;
            m_numNodes++;
            m_nodes[m_numNodes] = a_node;
            a_node.QueueIndex = m_numNodes;
            a_node.InsertionIndex = m_numNodesEverEnqueued++;
            CascadeUp(m_nodes[m_numNodes]);
        }


        #if NET_VERSION_4_5
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        #endif
        private void Swap(T a_node1, T a_node2)
        {
            //Swap the nodes
            m_nodes[a_node1.QueueIndex] = a_node2;
            m_nodes[a_node2.QueueIndex] = a_node1;

            //Swap their indicies
            int temp = a_node1.QueueIndex;
            a_node1.QueueIndex = a_node2.QueueIndex;
            a_node2.QueueIndex = temp;
        }


        //Performance appears to be slightly better when this is NOT inlined o_O
        private void CascadeUp(T a_node)
        {
            //aka Heapify-up
            int parent = a_node.QueueIndex / 2;
            while(parent >= 1)
            {
                T parentNode = m_nodes[parent];
                if(HasHigherPriority(parentNode, a_node))
                    break;

                //Node has lower priority value, so move it up the heap
                Swap(a_node, parentNode); //For some reason, this is faster with Swap() rather than (less..?) individual operations, like in CascadeDown()

                parent = a_node.QueueIndex / 2;
            }
        }


        #if NET_VERSION_4_5
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        #endif
        private void CascadeDown(T a_node)
        {
            //aka Heapify-down
            T newParent;
            int finalQueueIndex = a_node.QueueIndex;
            while(true)
            {
                newParent = a_node;
                int childLeftIndex = 2 * finalQueueIndex;

                //Check if the left-child is higher-priority than the current node
                if(childLeftIndex > m_numNodes)
                {
                    //This could be placed outside the loop, but then we'd have to check newParent != node twice
                    a_node.QueueIndex = finalQueueIndex;
                    m_nodes[finalQueueIndex] = a_node;
                    break;
                }

                T childLeft = m_nodes[childLeftIndex];
                if(HasHigherPriority(childLeft, newParent))
                {
                    newParent = childLeft;
                }

                //Check if the right-child is higher-priority than either the current node or the left child
                int childRightIndex = childLeftIndex + 1;
                if(childRightIndex <= m_numNodes)
                {
                    T childRight = m_nodes[childRightIndex];
                    if(HasHigherPriority(childRight, newParent))
                    {
                        newParent = childRight;
                    }
                }

                //If either of the children has higher (smaller) priority, swap and continue cascading
                if(newParent != a_node)
                {
                    //Move new parent to its new index.  node will be moved once, at the end
                    //Doing it this way is one less assignment operation than calling Swap()
                    m_nodes[finalQueueIndex] = newParent;

                    int temp = newParent.QueueIndex;
                    newParent.QueueIndex = finalQueueIndex;
                    finalQueueIndex = temp;
                }
                else
                {
                    //See note above
                    a_node.QueueIndex = finalQueueIndex;
                    m_nodes[finalQueueIndex] = a_node;
                    break;
                }
            }
        }


        /// <summary>
        /// Returns true if 'higher' has higher priority than 'lower', false otherwise.
        /// Note that calling HasHigherPriority(node, node) (ie. both arguments the same node) will return false
        /// </summary>
        #if NET_VERSION_4_5
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        #endif
        private bool HasHigherPriority(T a_higher, T a_lower)
        {
            return (a_higher.Priority < a_lower.Priority ||
                (a_higher.Priority == a_lower.Priority && a_higher.InsertionIndex < a_lower.InsertionIndex));
        }


        /// <summary>
        /// Removes the head of the queue (node with highest priority; ties are broken by order of insertion), and returns it.  O(log n)
        /// </summary>
        public T Dequeue()
        {
            T returnMe = m_nodes[1];
            Remove(returnMe);
            return returnMe;
        }


        /// <summary>
        /// Returns the head of the queue, without removing it (use Dequeue() for that).  O(1)
        /// </summary>
        public T First
        {
            get
            {
                return m_nodes[1];
            }
        }


        /// <summary>
        /// This method must be called on a node every time its priority changes while it is in the queue.  
        /// <b>Forgetting to call this method will result in a corrupted queue!</b>
        /// O(log n)
        /// </summary>
        #if NET_VERSION_4_5
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        #endif
        public void UpdatePriority(T a_node, double a_priority)
        {
            a_node.Priority = a_priority;
            OnNodeUpdated(a_node);
        }


        private void OnNodeUpdated(T a_node)
        {
            //Bubble the updated node up or down as appropriate
            int parentIndex = a_node.QueueIndex / 2;
            T parentNode = m_nodes[parentIndex];

            if(parentIndex > 0 && HasHigherPriority(a_node, parentNode))
            {
                CascadeUp(a_node);
            }
            else
            {
                //Note that CascadeDown will be called if parentNode == node (that is, node is the root)
                CascadeDown(a_node);
            }
        }


        /// <summary>
        /// Removes a node from the queue.  Note that the node does not need to be the head of the queue.  O(log n)
        /// </summary>
        public void Remove(T a_node)
        {
            if (!Contains(a_node)) {
              return;
            }
            
            if(m_numNodes <= 1)
            {
                m_nodes[1] = null;
                m_numNodes = 0;
                return;
            }

            //Make sure the node is the last node in the queue
            bool wasSwapped = false;
            T formerLastNode = m_nodes[m_numNodes];
            if(a_node.QueueIndex != m_numNodes)
            {
                //Swap the node with the last node
                Swap(a_node, formerLastNode);
                wasSwapped = true;
            }

            m_numNodes--;
            m_nodes[a_node.QueueIndex] = null;

            if(wasSwapped)
            {
                //Now bubble formerLastNode (which is no longer the last node) up or down as appropriate
                OnNodeUpdated(formerLastNode);
            }
        }


        public IEnumerator<T> GetEnumerator()
        {
            for(int i = 1; i <= m_numNodes; i++)
                yield return m_nodes[i];
        }


        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }


        /// <summary>
        /// <b>Should not be called in production code.</b>
        /// Checks to make sure the queue is still in a valid state.  Used for testing/debugging the queue.
        /// </summary>
        public bool IsValidQueue()
        {
            for(int i = 1; i < m_nodes.Length; i++)
            {
                if(m_nodes[i] != null)
                {
                    int childLeftIndex = 2 * i;
                    if(childLeftIndex < m_nodes.Length && m_nodes[childLeftIndex] != null && HasHigherPriority(m_nodes[childLeftIndex], m_nodes[i]))
                        return false;

                    int childRightIndex = childLeftIndex + 1;
                    if(childRightIndex < m_nodes.Length && m_nodes[childRightIndex] != null && HasHigherPriority(m_nodes[childRightIndex], m_nodes[i]))
                        return false;
                }
            }
            return true;
        }
    }
}