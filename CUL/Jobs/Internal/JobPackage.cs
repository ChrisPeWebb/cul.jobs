﻿using CUL.Jobs.Collections;

namespace CUL.Jobs.Internal
{
    /// <summary>
    /// Contains all of the information a JobThread requires to execute a job.
    /// </summary>
    public class JobPackage : PriorityQueueNode
    {
        public IJob Job { get; private set; }

        public OnJobCompleteEvent CompleteEvent { get; private set; }

        public bool ReturnOnMainThread { get; private set; }

        public bool ExceptionHandling { get; private set; }


        public JobPackage(IJob a_job, OnJobCompleteEvent a_completedEvent, bool a_returnOnMainThread, bool a_exceptionHandling)
        {
            Job = a_job;
            CompleteEvent = a_completedEvent;
            ExceptionHandling = a_exceptionHandling;
            ReturnOnMainThread = a_returnOnMainThread;
        }

    }
}
