﻿using UnityEngine;
using System;
using System.Collections;
using System.Threading;

namespace CUL.Jobs.Internal
{
    /// <summary>
    /// Contains everything needed to dispatch a simple or complex delegate to the main thread. Also handles the dispatching itself.
    /// </summary>
    public class ThreadDispatchPackage
    {
        public bool HasExecuted { get; private set; }

        public object ExecutionResult { get; private set; }

        private DispatchDelegate m_dispatchDelegate;
       
        private ParameterizedDispatchDelegate m_parameterizedDispatchDelegate;
        
        private ReturningDispatchDelegate m_returningDispatchDelegate;
        
        private ParameterizedReturningDispatchDelegate m_parameterizedReturningDispatchDelegate;

        private object m_dispatchArgument;

        private bool m_exceptionHandling;


        public ThreadDispatchPackage(DispatchDelegate a_dispatchDelegate, bool a_exceptionHandling)
        {
            m_exceptionHandling = a_exceptionHandling;
            m_dispatchDelegate = a_dispatchDelegate;
        }


        public ThreadDispatchPackage(ParameterizedDispatchDelegate a_dispatchDelegate, object a_argument, bool a_exceptionHandling)
        {
            m_exceptionHandling = a_exceptionHandling;
            m_parameterizedDispatchDelegate = a_dispatchDelegate;
            m_dispatchArgument = a_argument;
        }

        public ThreadDispatchPackage(ReturningDispatchDelegate a_dispatchDelegate, bool a_exceptionHandling)
        {
            m_exceptionHandling = a_exceptionHandling;
            m_returningDispatchDelegate = a_dispatchDelegate;
        }

        public ThreadDispatchPackage(ParameterizedReturningDispatchDelegate a_dispatchDelegate, object a_argument, bool a_exceptionHandling)
        {
            m_exceptionHandling = a_exceptionHandling;
            m_parameterizedReturningDispatchDelegate = a_dispatchDelegate;
            m_dispatchArgument = a_argument;
        }


        /// <summary>
        /// The current thread (if not the main thread) will be slept until execution is complete.
        /// </summary>
        public void WaitForExecution()
        {
            if(!MainThreadWatchdog.CheckIfMainThread())
            {
                while(!HasExecuted) // TODO: Check for unity activity
                {
                    Thread.Sleep(5);
                }
            }
        }


        /// <summary>
        /// The dispatch package will be executed.
        /// </summary>
        public void Execute()
        {
            // TODO: Is there any reason this needs exception handling? Shouldnt it already be on the main thread? How does an exeption here impact the rest?
            if(m_exceptionHandling)
            {
                try
                {
                    if(m_dispatchDelegate != null)
                    {
                        m_dispatchDelegate();
                    }
                    else if(m_parameterizedDispatchDelegate != null)
                    {
                        m_parameterizedDispatchDelegate(m_dispatchArgument);
                    }
                    else if(m_returningDispatchDelegate != null)
                    {
                        ExecutionResult = m_returningDispatchDelegate();
                    }
                    else if(m_parameterizedReturningDispatchDelegate != null)
                    {
                        ExecutionResult = m_parameterizedReturningDispatchDelegate(m_dispatchArgument);
                    }
                }
                catch(Exception ex)
                {
                    UnityEngine.Debug.LogException(ex);
                }
            }
            else
            {
                if(m_dispatchDelegate != null)
                {
                    m_dispatchDelegate();
                }
                else if(m_parameterizedDispatchDelegate != null)
                {
                    m_parameterizedDispatchDelegate(m_dispatchArgument);
                }
                else if(m_returningDispatchDelegate != null)
                {
                    ExecutionResult = m_returningDispatchDelegate();
                }
                else if(m_parameterizedReturningDispatchDelegate != null)
                {
                    ExecutionResult = m_parameterizedReturningDispatchDelegate(m_dispatchArgument);
                }
            }

            HasExecuted = true;
        }
    }
}