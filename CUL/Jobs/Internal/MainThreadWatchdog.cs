﻿using System.Threading;

namespace CUL.Jobs.Internal
{
    /// <summary>
    /// Tracks the main(Unity) thread and allows a thread to check if it is the main(Unity) thread.
    /// </summary>
    public class MainThreadWatchdog
    {
        private static Thread m_mainThread = null;

        /// <summary>
        /// Initialized the watchdog. Should be called from the main thread only.
        /// </summary>
        public static void Initialize()
        {
            if(m_mainThread == null)
            {
                Thread currentThread = Thread.CurrentThread;

                // The unity main thread has an ID of 1 and should not be in the multithreaded apartment
                if(currentThread.ManagedThreadId == 1 && currentThread.GetApartmentState() != ApartmentState.MTA)
                {
                    m_mainThread = currentThread;
                }
            }
        }


        /// <summary>
        /// Checks if the current thread is the main(Unity) thread.
        /// </summary>
        /// <returns>Whether or not the current thread is the main thread.</returns>
        public static bool CheckIfMainThread()
        {
            Initialize();
            return Thread.CurrentThread == m_mainThread;
        }
    }
}
