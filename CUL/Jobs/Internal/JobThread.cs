﻿using System;
using System.Threading;
using System.Collections.Generic;

using CUL.Jobs.Collections;

namespace CUL.Jobs.Internal
{
    /// <summary>
    /// Manages a single thread and its execution/work consumption loop.
    /// </summary>
    public class JobThread 
    {
        /// <summary>
        /// Is the thread currently executing a job?
        /// </summary>
        public bool IsBusy
        {
            get
            {
                return m_jobPackage != null;
            }
        }

        public string JobName { get; private set; }

        private JobPackage m_jobPackage;

        private Thread m_thread;

        private BlockingQueue<JobPackage> m_jobQueue;

        private bool m_isThreadAlive;


        public JobThread(BlockingQueue<JobPackage> a_jobQueue)
        {
            m_jobQueue = a_jobQueue;

            m_isThreadAlive = true;

            // Start the job execution loop
            m_thread = new Thread(Execute);
            m_thread.IsBackground = true;
            m_thread.Name = "CUL Job Thread";

            m_thread.Start();
        }


        /// <summary>
        /// Requests the currently executing job be stopped.
        /// </summary>
        /// <param name="a_forceInterrupt">If forced, the thread will be interrupted and joined with the main thread.</param>
        public void StopJobExecution(bool a_forceInterrupt)
        {
            if(m_jobPackage != null)
            {
                m_jobPackage.Job.Abort();
            }

            m_isThreadAlive = false;

            if(a_forceInterrupt)
            {
                // Abort doesnt play nice with web player
                m_thread.Interrupt();
                m_thread.Join();
            }
        }


        private void Execute()
        {
                while(m_isThreadAlive && (m_jobPackage = m_jobQueue.Dequeue()) != null)
                {
                    JobName = m_jobPackage.Job.GetType().Name;

                    if(m_jobPackage.ExceptionHandling)
                    {
                        try
                        {
                            // Execute the packages work safely
                            m_jobPackage.Job.Execute();

                            // Fire the packages on complete if it exists
                            // This intentionally will not be fired if execution throws an exception.
                            if(m_jobPackage.CompleteEvent != null && m_isThreadAlive)
                            {
                                if(m_jobPackage.ReturnOnMainThread)
                                {
                                    // These need to be cached and captured, as the job package is nulled after execution. 
                                    // These need to exist for some time until they are fired on the main thread
                                    IJob job = m_jobPackage.Job;
                                    OnJobCompleteEvent completedEvent = m_jobPackage.CompleteEvent;

                                    MainThreadDispatcher.DispatchToMainThread(
                                    () =>
                                    {
                                        completedEvent(job);
                                        m_jobPackage.CompleteEvent(m_jobPackage.Job);
                                    });

                                }
                                else
                                {
                                    m_jobPackage.CompleteEvent(m_jobPackage.Job);
                                }
                            }
                        }
                        catch(Exception ex)
                        {
                            if(m_isThreadAlive)
                            {
                                UnityEngine.Debug.LogException(ex);
                            }
                        }
                    }
                    else
                    {
                        // Execute the packages work
                        m_jobPackage.Job.Execute();

                        // Fire the packages on complete if it exists
                        if(m_jobPackage.CompleteEvent != null && m_isThreadAlive)
                        {
                            if(m_jobPackage.ReturnOnMainThread)
                            {
                                MainThreadDispatcher.DispatchToMainThread(
                                () =>
                                {
                                    m_jobPackage.CompleteEvent(m_jobPackage.Job);
                                });
                            }
                            else
                            {
                                m_jobPackage.CompleteEvent(m_jobPackage.Job);
                            }
                        }
                    }

                    m_jobPackage = null;

                    JobName = "";
                }
        }
    }
}