﻿using System.Threading;
using System.Collections.Generic;

namespace CUL.Jobs.Internal
{
    public delegate void DispatchDelegate();

    public delegate void ParameterizedDispatchDelegate(object a_argument);

    public delegate object ReturningDispatchDelegate();

    public delegate object ParameterizedReturningDispatchDelegate(object a_argument);


    /// <summary>
    /// Handles dispatching delegates to the main thread.
    /// </summary>
    public static class MainThreadDispatcher
    {
        private static List<ThreadDispatchPackage> m_dispatchPackages = new List<ThreadDispatchPackage>();


        /// <summary>
        /// Dispatches a delegate to the main thread. The current thread can be made to wait until execution has completed before continuing.
        /// </summary>
        /// <param name="a_dispatchDelegate">The delegate to execute on the main thread.</param>
        /// <param name="a_waitForExecution">The current thread will be slept until execution of the delegate has completed.</param>
        /// <param name="a_exceptionHandling">Execution of the delegate will be wraped in a try/catch block, and any exceptions will be dispatched to unity.</param>
        public static void DispatchToMainThread(DispatchDelegate a_dispatchDelegate, bool a_waitForExecution = false, bool a_exceptionHandling = true)
        {
            if(MainThreadWatchdog.CheckIfMainThread())
            {
                if(a_dispatchDelegate != null)
                {
                    a_dispatchDelegate();
                }
            }
            else
            {
                ThreadDispatchPackage dispatchPackage = new ThreadDispatchPackage(a_dispatchDelegate, a_exceptionHandling);

                lock(m_dispatchPackages)
                {
                    m_dispatchPackages.Add(dispatchPackage);
                }

                if(a_waitForExecution)
                {
                    // If the thread has asked for it, sleep it until the dispatch has been complete
                    dispatchPackage.WaitForExecution();
                }
            }
        }


        /// <summary>
        /// Dispatches a parameterized delegate to the main thread. The current thread can be made to wait until execution has completed before continuing.
        /// </summary>
        /// <param name="a_dispatchDelegate">The delegate to execute on the main thread.</param>
        /// <param name="a_argument">The argument which will be passed to the delegate.</param>
        /// <param name="a_waitForExecution">The current thread will be slept until execution of the delegate has completed.</param>
        /// <param name="a_exceptionHandling">Execution of the delegate will be wraped in a try/catch block, and any exceptions will be dispatched to unity.</param>
        public static void DispatchToMainThread(ParameterizedDispatchDelegate a_dispatchDelegate, object a_argument, bool a_waitForExecution = false, bool a_exceptionHandling = true)
        {
            if(MainThreadWatchdog.CheckIfMainThread())
            {
                if(a_dispatchDelegate != null)
                {
                    a_dispatchDelegate(a_argument);
                }
            }
            else
            {
                ThreadDispatchPackage dispatchPackage = new ThreadDispatchPackage(a_dispatchDelegate, a_argument, a_exceptionHandling);

                lock(m_dispatchPackages)
                {
                    m_dispatchPackages.Add(dispatchPackage);
                }

                if(a_waitForExecution)
                {
                    // If the thread has asked for it, sleep it until the dispatch has been complete
                    dispatchPackage.WaitForExecution();
                }
            }
        }


        /// <summary>
        /// Dispatches a returning delegate to the main thread. The current thread will be made to wait until execution has completed before continuing.
        /// </summary>
        /// <param name="a_dispatchDelegate">The delegate to execute on the main thread.</param>
        /// <param name="a_exceptionHandling">Execution of the delegate will be wraped in a try/catch block, and any exceptions will be dispatched to unity.</param>
        /// <returns>The result of the delegates execution.</returns>
        public static object DispatchToMainThreadReturn(ReturningDispatchDelegate a_dispatchDelegate, bool a_exceptionHandling = true)
        {
            if(MainThreadWatchdog.CheckIfMainThread())
            {
                if(a_dispatchDelegate != null)
                {
                    return a_dispatchDelegate();
                }
                else
                {
                    return null;
                }
            }
            else
            {
                ThreadDispatchPackage dispatchPackage = new ThreadDispatchPackage(a_dispatchDelegate, a_exceptionHandling);

                lock(m_dispatchPackages)
                {
                    m_dispatchPackages.Add(dispatchPackage);
                }

                // When returning, we always want to wait for the return to be created, then return it
                dispatchPackage.WaitForExecution();

                return dispatchPackage.ExecutionResult;
            }
        }


        /// <summary>
        /// Dispatches a returning parameterized delegate to the main thread. The current thread will be made to wait until execution has completed before continuing.
        /// </summary>
        /// <param name="a_dispatchDelegate">The delegate to execute on the main thread.</param>
        /// <param name="a_argument">The argument which will be passed to the delegate.</param>
        /// <param name="a_exceptionHandling">Execution of the delegate will be wraped in a try/catch block, and any exceptions will be dispatched to unity.</param>
        /// <returns>The result of the delegates execution.</returns>
        public static object DispatchToMainThreadReturn(ParameterizedReturningDispatchDelegate a_dispatchDelegate, object a_argument, bool a_exceptionHandling = true)
        {
            if(MainThreadWatchdog.CheckIfMainThread())
            {
                if(a_dispatchDelegate != null)
                {
                    return a_dispatchDelegate(a_argument);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                ThreadDispatchPackage dispatchPackage = new ThreadDispatchPackage(a_dispatchDelegate, a_argument, a_exceptionHandling);

                lock(m_dispatchPackages)
                {
                    m_dispatchPackages.Add(dispatchPackage);
                }

                // When returning, we always want to wait for the return to be created, then return it
                dispatchPackage.WaitForExecution();

                return dispatchPackage.ExecutionResult;
            }
        }


        /// <summary>
        /// Processes any currently queued dispatches. Should only be called on the main thread.
        /// </summary>
        public static void ProcessDispatchPackages()
        {
            if(MainThreadWatchdog.CheckIfMainThread())
            {
                if(m_dispatchPackages != null && m_dispatchPackages.Count > 0)
                {
                    lock(m_dispatchPackages)
                    {
                        for( int i = 0; i < m_dispatchPackages.Count; ++i)
                        { 
                            if(!m_dispatchPackages[i].HasExecuted)
                            {
                                m_dispatchPackages[i].Execute();
                            }
                        }

                        m_dispatchPackages.Clear();
                        Monitor.PulseAll(m_dispatchPackages);
                    }
                }
            }
        }

    }
}
