﻿using UnityEngine;
using System.Collections;

namespace CUL.Jobs
{
    public class JobManagerGUI : MonoBehaviour
    {
        [SerializeField]
        private bool m_enableDebugGUI = true;

        [SerializeField]
        private bool m_displayJobThreads = true;

        private int m_usageRecordFrames = 50;

        private GUIStyle m_debugStyle = new GUIStyle();

        private Texture2D m_busyTexture;
        private Texture2D m_idleTexture;

        private bool m_texturesCreated = false;

        private float[] m_usageHistory;

        private float m_currentUsage;
        private float m_historyUsage;

        private int m_currentHistoryIndex;

        private void Awake()
        {
            m_busyTexture = new Texture2D(1, 1);
            m_idleTexture = new Texture2D(1, 1);

            m_usageHistory = new float[m_usageRecordFrames];
        }


        private void Update()
        {
            m_currentUsage = JobManager.Instance.GetBusyThreadsCount() / (float)JobManager.Instance.JobThreadCount;

            m_usageHistory[m_currentHistoryIndex] = m_currentUsage;

            ++m_currentHistoryIndex;

            if(m_currentHistoryIndex >= m_usageRecordFrames)
            {
                m_currentHistoryIndex = 0;
            }

            m_historyUsage = 0;
            for(int i = 0; i < m_usageRecordFrames; ++i)
            {
                m_historyUsage += m_usageHistory[i];
            }

            m_historyUsage /= m_usageRecordFrames;
        }


        private void OnGUI()
        {
            if(!m_texturesCreated)
            {
                m_busyTexture.SetPixel(0, 0, Color.red);
                m_busyTexture.Apply();

                m_idleTexture.SetPixel(0, 0, Color.green);
                m_idleTexture.Apply();

                m_texturesCreated = true;
            }
            if(m_enableDebugGUI)
            {
                int jobThreads = JobManager.Instance.JobThreadCount;

                bool[] busyThreads = JobManager.Instance.GetBusyThreads();
                string[] jobNames = JobManager.Instance.GetJobNames();

                float boxHeight = 20.0f;

                Color originalColor = GUI.color;
                Color originalBackgroundColor = GUI.backgroundColor;
                Color originalContentColor = GUI.contentColor;

                if(m_displayJobThreads)
                {
                    GUI.Box(new Rect(0.0f, 0.0f, Screen.width, boxHeight), "");

                    GUILayout.BeginHorizontal();

                    float step = Screen.width / jobThreads;
                    float padding = 2.0f;
                    float currentMinX = padding;
                    float currentMaxX = step;

                    for(int i = 0; i < jobThreads; ++i)
                    {
                        string threadState = "";

                        if(busyThreads[i])
                        {
                            threadState = jobNames[i];
                            GUI.contentColor = Color.black;
                            m_debugStyle.normal.background = m_busyTexture;
                        }
                        else
                        {
                            threadState = "Idle";
                            GUI.contentColor = Color.black;
                            m_debugStyle.normal.background = m_idleTexture;
                        }

                        GUI.skin.label.alignment = TextAnchor.MiddleCenter;
                        GUI.Box(new Rect(currentMinX, padding, step - padding, boxHeight - padding * 2.0f), "", m_debugStyle);
                        GUI.Label(new Rect(currentMinX, padding, step - padding, boxHeight - padding * 2.0f), threadState);
                        GUI.skin.label.alignment = TextAnchor.MiddleLeft;

                        currentMinX += step;
                        currentMaxX += step;

                        GUI.contentColor = originalContentColor;
                        GUI.color = originalColor;
                        GUI.backgroundColor = originalBackgroundColor;
                    }
                    GUILayout.EndHorizontal();
                }
                else
                {
                    boxHeight = 0.0f;
                }

                GUILayout.BeginArea(new Rect(10, boxHeight + 10, 180, 300));
                GUILayout.BeginVertical("Box");
                //GUILayout.Label("Current Usage: " + (m_currentUsage * 100.0f).ToString("0.0") + "%");
                //GUILayout.Label("Average Usage: " + (m_historyUsage * 100.0f).ToString("0.0") + "%");

                    
                // cheekyly get a nicer interploated color (interpolated then normalized to stop the color from getting too dark in between)
                Color mix = Color.Lerp(Color.green, Color.red, m_historyUsage);
                Vector3 colorVec = new Vector3(mix.r, mix.g, mix.b);
                colorVec.Normalize();
                GUI.contentColor = new Color(colorVec.x,colorVec.y,colorVec.z);

                GUI.skin.label.fontStyle = FontStyle.Bold;

                GUILayout.Label("Job Thread Usage: " + (m_historyUsage * 100.0f).ToString("0.0") + "%");
                GUI.contentColor = originalContentColor;
                GUILayout.Label("Job Queue Size: " + JobManager.Instance.JobCount);
                GUI.skin.label.fontStyle = FontStyle.Normal;
                GUILayout.EndVertical();
                GUILayout.EndArea();


                GUI.color = originalColor;
                GUI.backgroundColor = originalBackgroundColor;
                GUI.contentColor = originalContentColor;
            }
        }
    }
}
